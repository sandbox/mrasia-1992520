<?php
/**
 * @file
 * cart_items.inc
 */

/**
 * Create default cart items component.
 */
function _webform_defaults_cart_items() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'mandatory' => 0,
    'extra' => array(
      'hidden' => 0,
    ),
  );
}

/**
 * Implementation of _webform_theme_component().
 */
function _webform_theme_cart_items() {
  return array(
    'webform_display_cart_items' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implementation of _webform_edit_component().
 */
function _webform_edit_cart_items($component) {
  $form = array();

  $form['extra']['hidden'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hidden'),
    '#attributes' => (!empty($component['extra']['hidden'])) ? array('checked' => array('checked')) : array(),
    '#description' => t('Hide the cart on the form'),
  );

  return $form;
}

/**
 * Implementation of _webform_render_component().
 */
function _webform_render_cart_items($component, $value = NULL, $filter = TRUE) {
  $form_item = array(
    '#type' => 'hidden',
  );

  // Current order uses view.
  if ($value) {
    $form_item['#prefix'] = _webform_table_cart_items($value[0]);
  }
  else {
    global $user;
    // First check to make sure we have a valid order.
    if ($order = commerce_cart_order_load($user->uid)) {
      $wrapper = entity_metadata_wrapper('commerce_order', $order);
      // Only show if we found product line items.
      if (commerce_line_items_quantity($wrapper->commerce_line_items, commerce_product_line_item_types()) > 0) {
        if (!$component['extra']['hidden']){
          $view = views_get_view('webform_cart_summary');
          $view->set_display('default');
          $view->set_arguments(array($order->order_id));
          $view->pre_execute();
          $view->execute();
          $content = $view->render();
          $form_item['#prefix'] = $content;
        }
      }
    }
  }

  return $form_item;
}

/**
 * Return cart items for display. (email, submission view).
 */
function _webform_display_cart_items($component, $value, $format = 'html') {
  $table = _webform_table_cart_items($value[0], $format);
  $format = '';

  return array(
    '#title' => $component['name'],
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_textfield',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#post_render' => array('webform_element_wrapper'),
    '#field_prefix' => '',
    '#field_suffix' => '',
    '#component' => $component,
    '#format' => $format,
    '#value' => $table,
  );
}

/**
 * Format the cart contents.
 */
function _webform_submit_cart_items($component, $value) {
  global $user;

  // First check to make sure we have a valid order.
  if ($order = commerce_cart_order_load($user->uid)) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    // Only show if we found product line items.
    if (commerce_line_items_quantity($wrapper->commerce_line_items, commerce_product_line_item_types()) > 0) {
      // Get the cart contents.
      $view = views_get_view('commerce_cart_block');
      $view->set_arguments(array($order->order_id));
      $view->execute();

      // Format the cart contents into a format for saving to the webform results.
      $cart_items = array();
      foreach ($view->result as $view_item) {
        $cart_items[] = $view_item->_field_data['commerce_line_item_field_data_commerce_line_items_line_item_']['entity'];
      }
    }
  }

  return serialize($cart_items);
}

/**
 * Render a product table.
 */
function _webform_table_cart_items($items, $format = 'html') {
  $data = unserialize($items); 
  $val = '';

  $header = array('SKU', 'Product', 'Quantity');
  $rows = array();

  if ($data) {
    foreach ($data as $item) {
      $row = array();
      $row[] = $item->line_item_label;

      if (isset($item->data['context']['product_ids'][0])) {
        $product_display = commerce_product_load($item->data['context']['product_ids'][0]);
        // @todo, add proper product link.
        $row[] = l(t($product_display->title), '#');
      }
      $row[] = array('data' => number_format($item->quantity), 'style' => array('text-align: center'));
      $rows[] = $row;
    }
  }

  // HTML must be declared as send method.
  if ($format == 'html') {
    $val = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('style' => array('width: 100%;'))));
    // Checkplain is not used in webform_display_textfield.
    $format = '';
  } 
  else {
    foreach ($rows as $row) {
      $val .= $row[2] . ' x ' . strip_tags($row[0]) . "\n";
    }
  }

  return $val;
}
