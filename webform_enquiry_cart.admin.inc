<?php
/**
 * @file
 * webform_enquiry_cart.admin.inc
 */
 
/**
 * Menu callback for admin settings
 */
function webform_enquiry_cart_admin_settings_form($form, $form_state) {
  // Load all menu(s) on site.
  $menu = menu_get_menus();
  // Load all webform(s) on site.
  $values = array();
  $webforms = _webform_enquiry_cart_node_load_by_type('webform');
  if ($webforms) {
    foreach ($webforms as $webform) {
      $values[$webform->nid] = $webform->title;
    }
  }

  // Additional submit callback for the menu parent.
  $form['#submit'][] = 'webform_enquiry_cart_admin_settings_form_submit';
  $form['#validate'][] = 'webform_enquiry_cart_admin_settings_form_validate';

  $form['webform_enquiry_cart_target_webform'] = array(
    '#type' => 'select',
    '#title' => t('Enquiry Cart Form'),
    '#options' => array(
      'Webform' => $values
    ),
    '#default_value' => variable_get('webform_enquiry_cart_target_webform'),
    '#description' => t('The target webform to enable as an enquiry cart form.'),
    '#required' => FALSE,
  );
  $form['webform_enquiry_cart_clear_cart'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clear Cart'),
    '#default_value' => variable_get('webform_enquiry_cart_clear_cart', 0),
    '#description' => t('Clear a users cart on enquiry form submission.'),
  );
  /*$form['webform_enquiry_cart_order_history'] = array(
    '#type' => 'checkbox',
    '#title' => t('Order History'),
    '#default_value' => variable_get('webform_enquiry_cart_order_history', 0),
    '#description' => t('Enter & keep enquiry form submissions as real orders.'),
  );*/
  $form['webform_enquiry_cart_menu_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Block Menu Item'),
    '#default_value' => variable_get('webform_enquiry_cart_menu_block', 0),
    '#description' => t('Enable an enquiry cart menu block.'),
  );
  $form['webform_enquiry_cart_menu_toggle'] = array(
    '#type' => 'checkbox',
    '#title' => t('Menu Item'),
    '#default_value' => variable_get('webform_enquiry_cart_menu_toggle', 0),
    '#description' => t('Enable an enquiry cart menu item.'),
  );
  $form['webform_enquiry_cart_menu_force_theme'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable theme_link'),
    '#states' => array(
      'visible' => array(
        ':input[name="webform_enquiry_cart_menu_toggle"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => variable_get('webform_enquiry_cart_menu_force_theme', 0),
    '#description' => t('Depending where your menu item resides theme_link might be required; warning could do unexpected things...'),
  );
  $form['webform_enquiry_cart_menu_parent'] = array(
    '#type' => 'select',
    '#states' => array(
      'visible' => array(
        ':input[name="webform_enquiry_cart_menu_toggle"]' => array('checked' => TRUE),
      ),
    ),
    '#options' => array(
      'Menu' => $menu,
    ),
    '#default_value' => variable_get('webform_enquiry_cart_menu_parent'),
    '#description' => t('Select a menu to enter the enquiry cart link into.'),
  );

  return system_settings_form($form);
}

/**
 * Validate callback for enquiry cart settings form.
 */
function webform_enquiry_cart_admin_settings_form_validate($form, &$form_state) {
  if (!isset($form_state['values']['webform_enquiry_cart_target_webform'])) {
    form_set_error('webform_enquiry_cart', t('No target Webform found.'));
  }
}

/**
 * Submit callback for enquiry cart settings form.
 */
function webform_enquiry_cart_admin_settings_form_submit($form, &$form_state) {
  $mlid = variable_get('webform_enquiry_cart_menu_mlid', 0);
  $parent = variable_get('webform_enquiry_cart_menu_parent');
  // Delete menu item.
  if ($form_state['values']['webform_enquiry_cart_menu_toggle'] == 0 || $form_state['values']['webform_enquiry_cart_menu_parent'] != $parent) {
    if ($mlid) {
      menu_link_delete($mlid);
      watchdog('webform_enquiry_cart', 'The menu item @mlid has been deleted.', array('@mlid' => $mlid), WATCHDOG_NOTICE);
      variable_del('webform_enquiry_cart_menu_mlid');
      cache_clear_all();
    }
  }
  // Add menu item.
  if (isset($form_state['values']['webform_enquiry_cart_menu_parent']) && $form_state['values']['webform_enquiry_cart_menu_toggle'] == 1) {
    $item = array(
      'link_path' => 'node/' . $form_state['values']['webform_enquiry_cart_target_webform'],
      'link_title' => t('Enquiry Cart'),
      'menu_name' => $form_state['values']['webform_enquiry_cart_menu_parent'],
      'weight' => 0,
      'mlid' => 0,
    );
    // Save enquiry cart link.
    if (!$mlid) {
      $mlid = menu_link_save($item);
      if ($mlid) {
        variable_set('webform_enquiry_cart_menu_mlid', $mlid);
        watchdog('webform_enquiry_cart', 'The menu item @mlid has been saved.', array('@mlid' => $mlid), WATCHDOG_NOTICE);
      }
    }
  }
}

/**
 * Helper function.
 *
 * @Description
 *  Implements node_load_multiple functionality with type condition.
 */
function _webform_enquiry_cart_node_load_by_type($type) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $type);
  $results = $query->execute();

  if ($results) {
    return node_load_multiple(array_keys($results['node']));
  }
}